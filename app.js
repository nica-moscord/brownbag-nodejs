const express = require("express");
const app = express();
const port = 3000;
const userRoute = require("./routes/users");
const forexRoute = require("./routes/forex");
const validatorMiddleware = require("./middlewares/validators");

//parse incoming request
app.use(express.json());
app.use(express.urlencoded());

app.use('/users', validatorMiddleware, userRoute);
app.use('/forex', forexRoute);

app.listen(port, () => console.log(`listening to port ${port}`));
