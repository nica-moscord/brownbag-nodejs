const greetUser = (req, res) => {
    res.send("Hello user! Welcome to express");
};

const greetSpecificUser = (req, res) => {
    console.log(req.params);
    res.send(`Nice to meet you user ${req.params.id}`);
}

const createUser = (req, res) => {
    const userInput = req.body;
    console.log(`USER INPUT`, userInput);
    res.status(200).send('successfully added user');
}

module.exports = {
    greetUser,
    greetSpecificUser,
    createUser
}