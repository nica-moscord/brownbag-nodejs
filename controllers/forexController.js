const request = require("request");
require('dotenv').config();

const getExchangeRates = (req, res) => {
    const forexURL = process.env.FOREX_URL;
    const forexAccessKey = process.env.FOREX_ACCESS_KEY;
    const api = `${forexURL}?access_key=${forexAccessKey}&symbols=USD,AUD,CAD,PLN,MXN,DKK,JPY&format=1`;

    request(api, function(error, response, body){
        if(!error && response.statusCode === 200) {
            const parsedBody = JSON.parse(body).rates;

            const result = Object.entries(parsedBody).map(([key, value]) => {
                return {
                    currency: key,
                    value
                }
            })

            return res.json({
                status: 200,
                message: 'successfully retrieved forex exchange',
                data: result
            })
        }
    })
}

module.exports = {
    getExchangeRates
}