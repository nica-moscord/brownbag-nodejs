const http = require('http');

const requestListener = function (req, res) {
    console.log('hello');
    res.writeHead(200);
    res.end('Hello world');
}

const server = http.createServer(requestListener);
server.listen(3000);