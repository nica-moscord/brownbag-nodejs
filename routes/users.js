const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");

router.get('/greet', userController.greetUser);
router.get('/greet/:id', userController.greetSpecificUser);
router.post('/', userController.createUser);

module.exports = router;