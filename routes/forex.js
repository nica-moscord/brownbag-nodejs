const express = require("express");
const router = express.Router();
const forexController = require("../controllers/forexController");

router.get('/', forexController.getExchangeRates);

module.exports = router;