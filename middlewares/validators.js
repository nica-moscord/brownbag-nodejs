const checkPayload = (req, res, next) => {
    console.log('>>> checking payload...');

    if(!('name' in req.body)){
        throw new Error('Invalid payload');
    }
    next();
}

module.exports = checkPayload;